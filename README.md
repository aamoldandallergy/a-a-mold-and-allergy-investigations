A&A Mold and Allergy Investigations provides mold inspection and mold testing services throughout San Diego County including Carlsbad, Chula Vista, Coronado, Encinitas and Oceanside. Our friendly and knowledgeable mold inspectors will answer your all questions and provide insights and guidance to give you peace of mind. We are BBB accredited (A+ rated), and people love us on Yelp! We can help you. Satisfaction Guaranteed.

Address : P.O. Box 28882, San Diego, CA 92198

Phone : 858-613-1042